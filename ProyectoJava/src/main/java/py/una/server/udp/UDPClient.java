package py.una.server.udp;


import java.io.*;
import java.net.*;

import py.una.entidad.Persona;
import py.una.entidad.PersonaJSON;
import py.una.entidad.Clima;
import py.una.entidad.ClimaJSON;

//lectura de archivo
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
 
import java.io.FileReader;
import java.util.Iterator;


class UDPClient {

    public static void main(String a[]) throws Exception {

        // Datos necesario
        String direccionServidor = "127.0.0.1";

        if (a.length > 0) {
            direccionServidor = a[0];
        }

        int puertoServidor = 9876;
        
        try {

        	//lectura desde archivo json
        	JSONParser parser = new JSONParser();

    			String name = new File(".").getCanonicalPath(); 
    			File file = new File(name, "/"+ "datos.json"); 
    			System.out.println("path "+file);
    			Object obj = parser.parse(new FileReader(file));
     
    			// A JSON object. Key value pairs are unordered. JSONObject supports java.util.Map interface.
    			JSONObject jsonObject = (JSONObject) obj;
     
    			// A JSON array. JSONObject supports java.util.List interface.
    			long id_es = (Long) jsonObject.get("id_estacion");
    			String ciu = (String) jsonObject.get("ciudad");
    			long porc = (Long) jsonObject.get("porcentaje_humedad");
    			long temp = (Long) jsonObject.get("temperatura");
    			long vel = (Long) jsonObject.get("velocidad_viento");
    			String fech = (String) jsonObject.get("fecha");
    			String hor = (String) jsonObject.get("hora");

        		
        	//acciones a realizar
            BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
            DatagramSocket clientSocket = new DatagramSocket();

            InetAddress IPAddress = InetAddress.getByName(direccionServidor);
            System.out.println("Intentando conectar a = " + IPAddress + ":" + puertoServidor +  " via UDP...");

            byte[] sendData = new byte[1024];
            byte[] receiveData = new byte[1024];

            //System.out.print("Ingrese el número de cédula (debe ser numérico): ");
            //String strcedula = inFromUser.readLine();
            //Long cedula = 0L;
            //try {
            //	cedula = Long.parseLong(strcedula);
            //}catch(Exception e1) {
            	
            //}
           
            
            //Persona p = new Persona(cedula, nombre, apellido);
            
            Clima p = new Clima(id_es,porc,temp,vel,fech,hor,ciu);
            String datoPaquete = ClimaJSON.objetoString(p); 
            //String datoPaquete = PersonaJSON.objetoString(p); 
            sendData = datoPaquete.getBytes();

            System.out.println("Enviar " + datoPaquete + " al servidor. ("+ sendData.length + " bytes)");
            DatagramPacket sendPacket =
                    new DatagramPacket(sendData, sendData.length, IPAddress, puertoServidor);

            clientSocket.send(sendPacket);

            DatagramPacket receivePacket =
                   new DatagramPacket(receiveData, receiveData.length);

            System.out.println("Esperamos si viene la respuesta.");

            //Vamos a hacer una llamada BLOQUEANTE entonces establecemos un timeout maximo de espera
            clientSocket.setSoTimeout(10000);

            try {
                //ESPERAMOS LA RESPUESTA, BLOQUENTE
                clientSocket.receive(receivePacket);

                String respuesta = new String(receivePacket.getData());
                Clima clima = ClimaJSON.stringObjeto(respuesta.trim());
                
                InetAddress returnIPAddress = receivePacket.getAddress();
                int port = receivePacket.getPort();

                System.out.println("Respuesta desde =  " + returnIPAddress + ":" + port);
                System.out.println("Datos del clima");
                System.out.println(clima.getCiudad());
                System.out.println(clima.getFecha());
                System.out.println(clima.getHora());
                System.out.println(clima.getId_estacion());
                System.out.println(clima.getPorcentaje_humedad());
                System.out.println(clima.getTemperatura());
                System.out.println(clima.getVelocidad_viento());
                
                                

            } catch (SocketTimeoutException ste) {

                System.out.println("TimeOut: El paquete udp se asume perdido.");
            }
            clientSocket.close();
        } catch (UnknownHostException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }
} 

