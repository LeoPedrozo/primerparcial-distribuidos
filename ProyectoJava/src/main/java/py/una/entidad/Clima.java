package py.una.entidad;

import java.util.ArrayList;

public class Clima {
	long id_estacion, porcentaje_humedad,temperatura;
	long velocidad_viento;
	String fecha,hora,ciudad;
	
	public Clima(){
	}
	
	public Clima(long id_estacion, long porcentaje_humedad, long temperatura, long velocidad_viento, String fecha,
			String hora, String ciudad) {
		super();
		this.id_estacion = id_estacion;
		this.porcentaje_humedad = porcentaje_humedad;
		this.temperatura = temperatura;
		this.velocidad_viento = velocidad_viento;
		this.fecha = fecha;
		this.hora = hora;
		this.ciudad = ciudad;
	}
	public long getId_estacion() {
		return id_estacion;
	}
	public void setId_estacion(long id_estacion) {
		this.id_estacion = id_estacion;
	}
	public long getPorcentaje_humedad() {
		return porcentaje_humedad;
	}
	public void setPorcentaje_humedad(long porcentaje_humedad) {
		this.porcentaje_humedad = porcentaje_humedad;
	}
	public long getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(long temperatura) {
		this.temperatura = temperatura;
	}
	public long getVelocidad_viento() {
		return velocidad_viento;
	}
	public void setVelocidad_viento(long velocidad_viento) {
		this.velocidad_viento = velocidad_viento;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}


}
