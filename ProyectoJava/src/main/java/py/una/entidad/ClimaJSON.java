package py.una.entidad;
import java.util.Iterator;

import py.una.entidad.Clima;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ClimaJSON {
    
    public static String objetoString(Clima p) {	
    	
		JSONObject obj = new JSONObject();
        obj.put("id_estacion", p.getId_estacion());
        obj.put("ciudad", p.getCiudad());
        obj.put("porcentaje_humedad", p.getPorcentaje_humedad());
        obj.put("temperatura", p.getTemperatura());
        obj.put("velocidad_viento", p.getVelocidad_viento());
        obj.put("fecha", p.getFecha());
        obj.put("hora", p.getHora());
        return obj.toJSONString();
    }
    
    public static Clima stringObjeto(String str) throws Exception {
    	Clima p = new Clima();
        JSONParser parser = new JSONParser();

        Object obj = parser.parse(str.trim());
        JSONObject jsonObject = (JSONObject) obj;
        
        Long id_est = (Long) jsonObject.get("id_estacion");
        p.setId_estacion(id_est);
        Long porc = (Long) jsonObject.get("porcentaje_humedad");
        p.setPorcentaje_humedad(porc);
        Long temp = (Long) jsonObject.get("temperatura");
        p.setTemperatura(temp);
        Long vel = (Long) jsonObject.get("velocidad_viento");
        p.setVelocidad_viento(vel);
        p.setFecha((String)jsonObject.get("fecha"));
        p.setHora((String)jsonObject.get("hora"));
        p.setCiudad((String)jsonObject.get("ciudad"));
        
        return p;
	}

}